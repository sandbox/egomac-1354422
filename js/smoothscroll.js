(function($) {
  Drupal.behaviors.smoothscroll = {
    attach: function(context) {
      $('a[href*=#]').smoothScroll();
    }
  }
})(jQuery);